package com.expenser.expenser.controller;

import com.expenser.expenser.constants.ExpenserConstants;
import com.expenser.expenser.metadata.RoleDto;
import com.expenser.expenser.metadata.UserDto;
import com.expenser.expenser.metadata.UserGroupDto;
import com.expenser.expenser.model.UserGroup;
import com.expenser.expenser.service.base.BaseService;
import com.expenser.expenser.util.Outputter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;
import java.util.List;

@Controller
@RequestMapping(ExpenserConstants.LOGGEDIN_REQUEST_MAPPING_PATTERN)
public class UserGroupController {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupController.class);

    @Autowired
    BaseService<UserGroupDto> userGroupService;

    /**
     * handles a POST request for creating an UserGroup
     * @param userGroupDto
     * @return ResponseEntity<UserDto>
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @RequestMapping(value = "/usergroup", method = RequestMethod.POST)
    @ApiOperation(value = "Define a new UserGroup", notes = "Creating a user group including the previously defined users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "{success: true, userGroup: XXXX}"),
            @ApiResponse(code = 400, message = "{success: false, message: Invalid User information provided}")
    })
    public ResponseEntity<JSONObject> save(@RequestBody UserGroupDto userGroupDto) {

        logger.info("in UserGroupController/usergroup POST method");

        if (userGroupDto == null) {

            JSONObject jsonObject = Outputter.create("Invalid UserGroup information provided",
                    HttpStatus.BAD_REQUEST.value(),
                    false,
                    null,
                    ""
                    );
            return new ResponseEntity<>(jsonObject, HttpStatus.BAD_REQUEST);
        }

        Serializable savedId = userGroupService.save(userGroupDto);
        userGroupDto.setId(Long.parseLong(savedId.toString()));

        JSONObject jsonObject = Outputter.create("",
                HttpStatus.CREATED.value(),
                true,
                userGroupDto,
                "userGroup"
        );

        return new ResponseEntity(jsonObject, HttpStatus.CREATED);
    }


    /**
     * handles a GET request for User list
     * @return ResponseEntity<List<UserDto>>
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @RequestMapping(value = "/usergroup", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "List all UserGroups", notes = "Returns a list of type UserGroupDto", response = UserGroupDto.class)
    public ResponseEntity<JSONObject> list() {

        logger.info("in UserGroupController/user GET method");

        List<UserGroupDto> userGroupDtos = userGroupService.list();
        logger.info("userGroupDtos: " + userGroupDtos);

        if (userGroupDtos == null) {
            JSONObject jsonObject = Outputter.create("No UserGroup found",
                    HttpStatus.NOT_FOUND.value(),
                    true,
                    userGroupDtos,
                    "userGroup"
            );
            return new ResponseEntity(jsonObject, HttpStatus.NOT_FOUND);
        }

        JSONObject jsonObject = Outputter.create("",
                HttpStatus.OK.value(),
                true,
                userGroupDtos,
                "userGroup"
        );

        return new ResponseEntity(jsonObject, (HttpStatus.valueOf((int)jsonObject.get("status_code"))));
    }



    /**
     * handles a PUT request for updating a UserGroup
     * @param userGroupDto
     * @return ResponseEntity<UserGroupDto>
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @RequestMapping(value = "/usergroup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
    @ApiOperation(value = "Update an existing UserGroup", notes = "Updating an existing UserGroup and returning the updated UserGroup in an object of type UserGroupDto", response = UserGroupDto.class)
    public ResponseEntity<JSONObject> update(@RequestBody UserGroupDto userGroupDto) {

        logger.info("in UserGroupController/user PUT method");

        if (userGroupDto == null) {

            JSONObject jsonObject = Outputter.create("Update failed.",
                    HttpStatus.BAD_REQUEST.value(),
                    false,
                    null,
                    "userGroup"
            );
            return new ResponseEntity(jsonObject, HttpStatus.BAD_REQUEST);
        }

        userGroupService.update(userGroupDto);

        JSONObject jsonObject = Outputter.create("",
                HttpStatus.CREATED.value(),
                true,
                userGroupDto,
                "userGroup"
        );

        return new ResponseEntity(jsonObject, HttpStatus.CREATED);
    }



    /**
     * handles a GET request for retrieving an UserGroup by its id
     * @param id
     * @return ResponseEntity<UserGroupDto>
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @RequestMapping(value = "/usergroup/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Find UserGroup by ID", notes = "Finding a UserGroup by input id and returning the result in an object of type UserGroupDto", response = UserGroupDto.class)
    public ResponseEntity<JSONObject> get(@PathVariable("id") String id) {

        logger.info("in UserGroupController/user/{" + id + "} GET method");

        UserGroupDto userGroupDto = userGroupService.get(id);

        if (userGroupDto == null) {

            JSONObject jsonObject = Outputter.create("UserGroup not found.",
                    HttpStatus.NOT_FOUND.value(),
                    false,
                    null,
                    ""
            );
            return new ResponseEntity(jsonObject, HttpStatus.NOT_FOUND);
        }

        JSONObject jsonObject = Outputter.create("",
                HttpStatus.OK.value(),
                true,
                userGroupDto,
                "userGroup"
        );
        return new ResponseEntity(jsonObject, HttpStatus.OK);
    }



    /**
     * handles a DELETE request for deleting a UserGroup by its id
     * @param id
     * @return ResponseEntity<UserGroupDto>
     */
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @RequestMapping(value = "/usergroup/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = "application/json")
    @ApiOperation(value = "Delete UserGroup by ID", notes = "Deleting an User with ID = id", response = UserGroupDto.class)
    public ResponseEntity<JSONObject> delete(@PathVariable("id") String id) {

        logger.info("in UserController/user/{" + id + "} DELETE method");

        UserGroupDto userGroupDto = userGroupService.get(id);

        if (userGroupDto == null) {

            JSONObject jsonObject = Outputter.create("UserGroup not found.",
                    HttpStatus.NOT_FOUND.value(),
                    false,
                    null,
                    ""
            );
            return new ResponseEntity(jsonObject, HttpStatus.NOT_FOUND);
        }
        userGroupService.delete(id);

        JSONObject jsonObject = Outputter.create("UserGroup deleted successfully.",
                HttpStatus.NO_CONTENT.value(),
                true,
                null,
                ""
        );
        return new ResponseEntity(jsonObject, HttpStatus.NO_CONTENT);
    }
}
