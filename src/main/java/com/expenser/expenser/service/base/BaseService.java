package com.expenser.expenser.service.base;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public interface BaseService<T> {
    public List<T> list();
    public Serializable save(T t);
    public void update(T t);
    public T get(String id);
    public void delete(String id);
}
