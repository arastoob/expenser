package com.expenser.expenser.service;

import com.expenser.expenser.metadata.RoleDto;
import com.expenser.expenser.metadata.UserGroupDto;
import com.expenser.expenser.model.Role;
import com.expenser.expenser.model.UserGroup;
import com.expenser.expenser.repository.base.BaseRepository;
import com.expenser.expenser.service.base.BaseService;
import com.expenser.expenser.util.objectMapper.base.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service("userGroupService")
public class UserGroupService implements BaseService<UserGroupDto> {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupService.class);

    @Autowired
    BaseRepository<UserGroup> userGroupDao;

    @Autowired
    BaseMapper<UserGroup, UserGroupDto> userGroupMapper;


    @Override
    public List<UserGroupDto> list() {
        logger.info("UserGroupService::list");

        return userGroupMapper.model2Dto(userGroupDao.list(), new ArrayList<UserGroupDto>());
    }

    @Override
    public Serializable save(UserGroupDto userGroupDto) {
        logger.info("UserGroupService::save: " + userGroupDto);

        UserGroup userGroup = userGroupMapper.dto2Model(userGroupDto, new UserGroup());
        return userGroupDao.save(userGroup);
    }

    @Override
    public void update(UserGroupDto userGroupDto) {
        logger.info("UserGroupService::update: " + userGroupDto);

        UserGroup userGroup = userGroupMapper.dto2Model(userGroupDto, new UserGroup());
        userGroupDao.update(userGroup);
    }

    @Override
    public UserGroupDto get(String id) {
        logger.info("UserGroupService::get:id: " + id);

        Long uuid = Long.parseLong(id);
        UserGroup userGroup = userGroupDao.get(uuid);
        if (userGroup != null)
            return userGroupMapper.model2Dto(userGroup, new UserGroupDto());
        return null;
    }

    @Override
    public void delete(String id) {
        logger.info("UserGroupService::delete:id: " + id);

        long uuid = Long.parseLong(id);
        userGroupDao.delete(uuid);
    }
}
