package com.expenser.expenser.service;

import com.expenser.expenser.metadata.RoleDto;
import com.expenser.expenser.model.Role;
import com.expenser.expenser.repository.base.BaseRepository;
import com.expenser.expenser.service.base.BaseService;
import com.expenser.expenser.util.objectMapper.base.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service("roleService")
public class RoleService implements BaseService<RoleDto> {

    private static final Logger logger = LoggerFactory.getLogger(RoleService.class);

    @Autowired
    private BaseRepository<Role> roleDao;

    @Autowired
    private BaseMapper<Role, RoleDto> roleMapper;


    @Override
    public List<RoleDto> list() {
        logger.info("RoleService::list");

        return roleMapper.model2Dto(roleDao.list(), new ArrayList<RoleDto>());
    }

    @Override
    public Serializable save(RoleDto roleDto) {
        logger.info("RoleService::save: " + roleDto);

        Role role = roleMapper.dto2Model(roleDto, new Role());
        return roleDao.save(role);
    }

    @Override
    public void update(RoleDto roleDto) {
        logger.info("RoleService::update: " + roleDto);

        Role role = roleMapper.dto2Model(roleDto, new Role());
        roleDao.update(role);
    }

    @Override
    public RoleDto get(String id) {
        logger.info("RoleService::get:id: " + id);

        Long uuid = Long.parseLong(id);
        Role role = roleDao.get(uuid);
        if (role != null)
            return roleMapper.model2Dto(role, new RoleDto());
        return null;
    }

    @Override
    public void delete(String id) {
        logger.info("RoleService::delete:id: " + id);

        long uuid = Long.parseLong(id);
        roleDao.delete(uuid);
    }
}
