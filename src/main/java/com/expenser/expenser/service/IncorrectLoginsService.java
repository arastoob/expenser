package com.expenser.expenser.service;

import com.expenser.expenser.metadata.IncorrectLoginsDto;
import com.expenser.expenser.model.IncorrectLogins;
import com.expenser.expenser.repository.IncorrectLoingsDao;
import com.expenser.expenser.repository.base.BaseRepository;
import com.expenser.expenser.service.base.BaseService;
import com.expenser.expenser.util.objectMapper.IncorrectLoginsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("incorrectLoginsService")
public class IncorrectLoginsService implements BaseService<IncorrectLoginsDto> {

    private static final Logger logger = LoggerFactory.getLogger(IncorrectLoginsService.class);

    @Autowired
    private BaseRepository<IncorrectLogins> incorrectLoginsDao;

    @Autowired
    private IncorrectLoginsMapper incorrectLoginsMapper;



    /**
     * retrieving a list of type LoginUser
     * @return List<IncorrectLoginsDto>
     */
    @Override
    public List<IncorrectLoginsDto> list() {
        logger.info("in IncorrectLoginsService: list");

        List<IncorrectLoginsDto> incorrectLoginsDtos = incorrectLoginsMapper.model2Dto(incorrectLoginsDao.list(),
                new ArrayList<IncorrectLoginsDto>());
        return incorrectLoginsDtos;
    }


    /**
     * creating a new IncorrectLogins
     * @param incorrectLoginsDto
     */
    @Override
    public Serializable save(IncorrectLoginsDto incorrectLoginsDto) {
        logger.info("in IncorrectLoginsService: save");

        IncorrectLogins incorrectLogins = incorrectLoginsMapper.dto2Model(incorrectLoginsDto, new IncorrectLogins());
        return incorrectLoginsDao.save(incorrectLogins);
    }


    /**
     * updating an existing IncorrectLogins
     * @param incorrectLoginsDto
     */
    @Override
    public void update(IncorrectLoginsDto incorrectLoginsDto) {
        logger.info("in IncorrectLoginsService: Update");

        IncorrectLogins incorrectLogins = incorrectLoginsMapper.dto2Model(incorrectLoginsDto, new IncorrectLogins());
        incorrectLoginsDao.update(incorrectLogins);
    }


    /**
     * retrieving a specific IncorrectLogins by its id
     * @param id
     * @return IncorrectLoginsDto
     */
    @Override
    public IncorrectLoginsDto get(String id) {
        logger.info("in IncorrectLoginsService: get");

        Long uuid = Long.parseLong(id);
        IncorrectLogins incorrectLogins = incorrectLoginsDao.get(uuid);
        if(incorrectLogins != null)
            return incorrectLoginsMapper.model2Dto(incorrectLogins, new IncorrectLoginsDto());
        return null;
    }


    /**
     * deleting a specific IncorrectLogins by its id
     * @param id
     */
    @Override
    public void delete(String id) {
        logger.info("in IncorrectLoginsService: delete");

        Long uuid = Long.parseLong(id);
        incorrectLoginsDao.delete(uuid);
    }


    /**
     * retrieving a specific IncorrectLogins by user id
     * @param userId
     * @return IncorrectLoginsDto
     */
    public IncorrectLoginsDto getByUserId(Long userId) {
        logger.info("in IncorrectLoginsService::getByUSerId:id: " + userId);

        List<IncorrectLogins> incorrectLoginsList = incorrectLoginsDao.list();
        List<IncorrectLogins> filteredByUserId = incorrectLoginsList.stream()
                .filter(incorrectLogins ->
                        incorrectLogins.getUser().getId() == userId
                        )
                .collect(Collectors.toList());
        if(!filteredByUserId.isEmpty())
            return incorrectLoginsMapper.model2Dto(filteredByUserId.get(0), new IncorrectLoginsDto());
        return null;
    }
}
