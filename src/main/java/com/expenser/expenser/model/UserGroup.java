package com.expenser.expenser.model;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "tbl_user_group")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false)
    private long id;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "address")
    private String address;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @Column(name = "users")
    private Set<User> users;

    @Column(name = "created_date")
    private Date createdDate;

    public UserGroup() {
    }

    public UserGroup(String groupName, String description, Set<User> users, String address, Date createdDate) {
        this.groupName = groupName;
        this.description = description;
        this.users = users;
        this.address = address;
        this.createdDate = createdDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * check to see whether users is loaded or not (for Lazy fetchType)
     * @return
     */
    public boolean isUsersLoaded() {
        return  Hibernate.isInitialized(this.users) && this.users != null;
    }


    @Override
    public String toString() {
        return "UserGroup{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", users=" + users +
                ", createdDate=" + createdDate +
                '}';
    }
}
