/*
 * Developed by Arastoo Bozorgi.
 * a.bozorgi67@gmail.com
 */

package com.expenser.expenser.model;

import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnTransformer;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tbl_user")
public class User {


    /**
     * attributes
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany
    @Column(name = "access_groups")
    private Set<AccessGroup> accessGroups;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "roles", nullable = true)
    private Set<Role> roles;

    @OneToMany
    @Column(name = "expenses", nullable = true)
    private Set<Expenses> expenses;

    @Column(name = "username")
    private String username;

    @ColumnTransformer(read = "pgp_sym_decrypt(password, 'a_strong_key')", write = "pgp_sym_encrypt(?, 'a_strong_key')")
    @Column(columnDefinition = "bytea")
    private String password;



    /**
     * constructors
     */
    public User() {
    }

    public User(String firstName, String lastName, Set<AccessGroup> accessGroups, Set<Role> roles, Set<Expenses> expenses,
                String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accessGroups = accessGroups;
        this.roles = roles;
        this.expenses = expenses;
        this.username = username;
        this.password = password;
    }

    /**
     * getter and setter methods
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<AccessGroup> getAccessGroups() {
        return accessGroups;
    }

    public void setAccessGroups(Set<AccessGroup> accessGroups) {
        this.accessGroups = accessGroups;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Expenses> getExpenses() {
        return expenses;
    }

    public void setExpenses(Set<Expenses> expenses) {
        this.expenses = expenses;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    /**
     * check to see whether expenses is loaded or not (for Lazy fetchType)
     * @return
     */
    public boolean isExpensesLoaded() {
        return  Hibernate.isInitialized(this.expenses) && this.expenses != null;
    }


    /**
     * check to see whether accessGroups is loaded or not (for Lazy fetchType)
     * @return
     */
    public boolean isAccesGroupsLoaded() {
        return  Hibernate.isInitialized(this.accessGroups) && this.accessGroups != null;
    }


    /**
     * check to see whether roles is loaded or not (for Lazy fetchType)
     * @return
     */
    public boolean isRolesLoaded() {
        return  Hibernate.isInitialized(this.roles) && this.roles != null;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accessGroupIDs=" + accessGroups +
                ", roleIDs=" + roles +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
