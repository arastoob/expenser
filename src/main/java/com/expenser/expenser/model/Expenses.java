package com.expenser.expenser.model;

import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_expenses")
public class Expenses {

    public enum paymentMethod {
        Visa,
        Debit,
        Cash;
        enum MasterCard {
            CapitalOne, Tangerine
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column(name = "description")
    private String description;

    @Column(name = "amount")
    private float amount;

    @Column(name = "date")
    private Date date;

    @Column(name = "payment_method")
    @Enumerated(EnumType.STRING)
    private paymentMethod paymentMethod;


    public Expenses() {
    }

    public Expenses(String description, float amount, Date date, paymentMethod paymentMethod) {
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.paymentMethod = paymentMethod;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Expenses.paymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Expenses.paymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    @Override
    public String toString() {
        return "Expenses{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", paymentMethod=" + paymentMethod +
                '}';
    }
}
