package com.expenser.expenser.util;

import com.expenser.expenser.constants.ExpenserConstants;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Outputter {

    private static final Logger logger = LoggerFactory.getLogger(Outputter.class);

    public static JSONObject create(String message, int statusCode, boolean success, Object object, String objectName) {
        logger.info("Outputter::create: message: " + message + ", statusCode: " + statusCode + ", success: " + success
                + ", objectName: " + objectName + ", object: " + object);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ExpenserConstants.SUCCESS_STR, success);
        if (!message.isEmpty() && message != "")
            jsonObject.put("message", message);
        jsonObject.put("status_code", statusCode);
        if (object != null)
            jsonObject.put(objectName, object);

        return jsonObject;
   }
}
