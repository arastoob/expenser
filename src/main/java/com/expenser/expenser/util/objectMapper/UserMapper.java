/*
 * Developed by Arastoo Bozorgi.
 * a.bozorgi67@gmail.com
 */

package com.expenser.expenser.util.objectMapper;

import com.expenser.expenser.metadata.AccessGroupDto;
import com.expenser.expenser.metadata.ExpensesDto;
import com.expenser.expenser.metadata.RoleDto;
import com.expenser.expenser.metadata.UserDto;
import com.expenser.expenser.model.AccessGroup;
import com.expenser.expenser.model.Expenses;
import com.expenser.expenser.model.Role;
import com.expenser.expenser.model.User;
import com.expenser.expenser.util.objectMapper.base.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * this class converts User to UserDto and vice versa
 */
@Component("userMapper")
public class UserMapper implements BaseMapper<User, UserDto> {

    private static final Logger logger = LoggerFactory.getLogger(UserMapper.class);

    /**
     * injecting AccessGroupMapper into this class
     */
    @Autowired
    AccessGroupMapper accessGroupMapper;

    /**
     * injecting RoleMapper into this class
     */
    @Autowired
    RoleMapper roleMapper;

    @Autowired
    ExpensesMapper expensesMapper;


    /**
     * converting User to UserDto
     * @param user
     * @param userDto
     * @return
     */
    @Override
    public UserDto model2Dto(User user, UserDto userDto) {
        logger.info("UserMapper::model2Dto");

        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());

        if(user.isAccesGroupsLoaded())
            userDto.setAccessGroupDtos(accessGroupMapper.model2Dto(user.getAccessGroups(), new HashSet<AccessGroupDto>()));

        if (user.isRolesLoaded())
            userDto.setRoleDtos(roleMapper.model2Dto(user.getRoles(), new HashSet<RoleDto>()));

        if (user.isExpensesLoaded())
            userDto.setExpensesDtos(expensesMapper.model2Dto(user.getExpenses(), new HashSet<ExpensesDto>()));

        return userDto;
    }


    /**
     * converting a list of User to a list of UserDto
     * @param users
     * @param userDtos
     * @return
     */
    @Override
    public List<UserDto> model2Dto(List<User> users, List<UserDto> userDtos) {
        logger.info("UserMapper::model2DtoList");

        for (User user: users){
            userDtos.add(model2Dto(user, new UserDto()));
        }
        return userDtos;
    }


    public Set<UserDto> model2Dto(Set<User> users, Set<UserDto> userDtos) {
        logger.info("UserMapper::model2DtoList");

        for (User user: users){
            userDtos.add(model2Dto(user, new UserDto()));
        }
        return userDtos;
    }


    /**
     * converting UserDto to User
     * @param userDto
     * @param user
     * @return
     */
    @Override
    public User dto2Model(UserDto userDto, User user) {
        logger.info("UserMapper::dto2Model");

        user.setId(userDto.getId());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());

        if (userDto.getAccessGroupDtos() != null)
            user.setAccessGroups(accessGroupMapper.dto2Model(userDto.getAccessGroupDtos(), new HashSet<AccessGroup>()));


        if (userDto.getRoleDtos() != null)
            user.setRoles(roleMapper.dto2Model(userDto.getRoleDtos(), new HashSet<Role>()));

        if (userDto.getExpensesDtos() != null)
            user.setExpenses(expensesMapper.dto2Model(userDto.getExpensesDtos(), new HashSet<Expenses>()));

        return user;
    }


    /**
     * converting a list of UserDto to a list of User
     * @param userDtos
     * @param users
     * @return
     */
    @Override
    public List<User> dto2Model(List<UserDto> userDtos, List<User> users) {
        logger.info("UserMapper::dto2ModelList");

        for (UserDto userDto:userDtos){
            users.add(dto2Model(userDto, new User()));
        }
        return users;
    }

    public Set<User> dto2Model(Set<UserDto> userDtos, Set<User> users) {
        logger.info("UserMapper::dto2ModelList");

        for (UserDto userDto:userDtos){
            users.add(dto2Model(userDto, new User()));
        }
        return users;
    }
}
