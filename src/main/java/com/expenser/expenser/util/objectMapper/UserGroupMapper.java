package com.expenser.expenser.util.objectMapper;

import com.expenser.expenser.metadata.*;
import com.expenser.expenser.model.User;
import com.expenser.expenser.model.UserGroup;
import com.expenser.expenser.util.objectMapper.base.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component("userGroupMapper")
public class UserGroupMapper implements BaseMapper<UserGroup, UserGroupDto> {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupMapper.class);

    @Autowired
    private UserMapper userMapper;


    @Override
    public UserGroupDto model2Dto(UserGroup userGroup, UserGroupDto userGroupDto) {
        logger.info("UserGroupMapper::model2Dto");

        userGroupDto.setId(userGroup.getId());
        userGroupDto.setAddress(userGroup.getAddress());
        userGroupDto.setCreatedDate(userGroup.getCreatedDate());
        userGroupDto.setDescription(userGroup.getDescription());
        userGroupDto.setGroupName(userGroup.getGroupName());

        if(userGroup.isUsersLoaded())
            userGroupDto.setUserDtos(userMapper.model2Dto(userGroup.getUsers(), new HashSet<UserDto>()));

        return userGroupDto;
    }

    @Override
    public List<UserGroupDto> model2Dto(List<UserGroup> userGroupList, List<UserGroupDto> userGroupDtoList) {
        logger.info("UserGroupMapper::model2DtoList");

        for (UserGroup userGroup: userGroupList){
            userGroupDtoList.add(model2Dto(userGroup, new UserGroupDto()));
        }
        return userGroupDtoList;
    }

    public Set<UserGroupDto> model2Dto(Set<UserGroup> userGroupList, Set<UserGroupDto> userGroupDtoList) {
        logger.info("UserGroupMapper::model2DtoList");

        for (UserGroup userGroup: userGroupList){
            userGroupDtoList.add(model2Dto(userGroup, new UserGroupDto()));
        }
        return userGroupDtoList;
    }

    @Override
    public UserGroup dto2Model(UserGroupDto userGroupDto, UserGroup userGroup) {
        logger.info("UserGroupMapper::dto2Model");

        userGroup.setId(userGroupDto.getId());
        userGroup.setAddress(userGroupDto.getAddress());
        userGroup.setCreatedDate(userGroupDto.getCreatedDate());
        userGroup.setDescription(userGroupDto.getDescription());
        userGroup.setGroupName(userGroupDto.getGroupName());

        if (userGroupDto.getUserDtos() != null)
            userGroup.setUsers(userMapper.dto2Model(userGroupDto.getUserDtos(), new HashSet<User>()));

        return userGroup;
    }

    @Override
    public List<UserGroup> dto2Model(List<UserGroupDto> userGroupDtoList, List<UserGroup> userGroupList) {
        logger.info("UserGroupMapper::dto2ModelList");

        for (UserGroupDto userGroupDto: userGroupDtoList){
            userGroupList.add(dto2Model(userGroupDto, new UserGroup()));
        }
        return userGroupList;
    }


    public Set<UserGroup> dto2Model(Set<UserGroupDto> userGroupDtoList, Set<UserGroup> userGroupList) {
        logger.info("UserGroupMapper::dto2ModelList");

        for (UserGroupDto userGroupDto: userGroupDtoList){
            userGroupList.add(dto2Model(userGroupDto, new UserGroup()));
        }
        return userGroupList;
    }
}
