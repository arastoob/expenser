package com.expenser.expenser.util.objectMapper;

import com.expenser.expenser.metadata.ExpensesDto;
import com.expenser.expenser.metadata.RoleDto;
import com.expenser.expenser.model.Expenses;
import com.expenser.expenser.model.Role;
import com.expenser.expenser.util.objectMapper.base.BaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component("expensesMapper")
public class ExpensesMapper implements BaseMapper<Expenses, ExpensesDto> {

    private static final Logger logger = LoggerFactory.getLogger(ExpensesMapper.class);

    @Override
    public ExpensesDto model2Dto(Expenses expenses, ExpensesDto expensesDto) {
        logger.info("ExpensesMapper::model2Dto");

        expensesDto.setId(expenses.getId());
        expensesDto.setAmount(expenses.getAmount());
        expensesDto.setDate(expenses.getDate());
        expensesDto.setDescription(expenses.getDescription());
        expensesDto.setPaymentMethod(expenses.getPaymentMethod());

        return expensesDto;
    }

    @Override
    public List<ExpensesDto> model2Dto(List<Expenses> expensesList, List<ExpensesDto> expensesDtoList) {
        logger.info("ExpensesMapper::model2DtoList");

        for (Expenses expenses: expensesList){
            expensesDtoList.add(model2Dto(expenses, new ExpensesDto()));
        }
        return expensesDtoList;
    }

    public Set<ExpensesDto> model2Dto(Set<Expenses> expensesList, Set<ExpensesDto> expensesDtoList) {
        logger.info("ExpensesMapper::model2DtoSet");

        for (Expenses expenses: expensesList){
            expensesDtoList.add(model2Dto(expenses, new ExpensesDto()));
        }
        return expensesDtoList;
    }

    @Override
    public Expenses dto2Model(ExpensesDto expensesDto, Expenses expenses) {
        logger.info("ExpensesMapper::dto2Model");

        expenses.setId(expensesDto.getId());
        expenses.setAmount(expensesDto.getAmount());
        expenses.setDate(expensesDto.getDate());
        expenses.setDescription(expensesDto.getDescription());
        expenses.setPaymentMethod(expensesDto.getPaymentMethod());

        return expenses;
    }

    @Override
    public List<Expenses> dto2Model(List<ExpensesDto> expensesDtoList, List<Expenses> expensesList) {
        logger.info("ExpensesMapper::dto2ModelList");

        for (ExpensesDto expensesDto: expensesDtoList){
            expensesList.add(dto2Model(expensesDto, new Expenses()));
        }
        return expensesList;
    }

    public Set<Expenses> dto2Model(Set<ExpensesDto> expensesDtoList, Set<Expenses> expensesList) {
        logger.info("ExpensesMapper::dto2ModelSet");

        for (ExpensesDto expensesDto: expensesDtoList){
            expensesList.add(dto2Model(expensesDto, new Expenses()));
        }
        return expensesList;
    }
}
