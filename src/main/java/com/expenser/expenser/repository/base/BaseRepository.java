package com.expenser.expenser.repository.base;

import java.util.List;

public interface BaseRepository<T> {

    public List<T> list();
    public Long save(T t);
    public void update(T t);
    public T get(Long id);
    public void delete(Long id);
}
