package com.expenser.expenser.repository;

import com.expenser.expenser.model.User;
import com.expenser.expenser.model.UserGroup;
import com.expenser.expenser.repository.base.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("userGroupDao")
@Transactional
public class UserGroupDao implements BaseRepository<UserGroup> {

    private static final Logger logger = LoggerFactory.getLogger(UserGroupDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<UserGroup> list() {
        logger.info("UserGroupDao::list");

        return entityManager.createQuery("from UserGroup").getResultList();
    }

    @Override
    public Long save(UserGroup userGroup) {
        logger.info("UserGroupDao::save: " + userGroup);

        entityManager.persist(userGroup);

        return userGroup.getId();
    }

    @Override
    public void update(UserGroup userGroup) {
        logger.info("UserGroupDao::update: " + userGroup);

        entityManager.merge(userGroup);
    }

    @Override
    public UserGroup get(Long id) {
        logger.info("UserGroupDao::get:id: " + id);

        return entityManager.find(UserGroup.class, id);
    }

    @Override
    public void delete(Long id) {
        logger.info("UserGroupDao::delete:id: " + id);

        UserGroup userGroup = entityManager.find(UserGroup.class, id);
        entityManager.remove(userGroup);
    }
}
