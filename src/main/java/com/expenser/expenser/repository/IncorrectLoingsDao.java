package com.expenser.expenser.repository;

import com.expenser.expenser.model.IncorrectLogins;
import com.expenser.expenser.repository.base.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("incorrectLoginsDao")
@Transactional
public class IncorrectLoingsDao implements BaseRepository<IncorrectLogins> {

    private static final Logger logger = LoggerFactory.getLogger(IncorrectLoingsDao.class);


    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<IncorrectLogins> list() {
        logger.info("IncorrectLoingsDao::list");

        return entityManager.createQuery("from IncorrectLogins").getResultList();
    }

    @Override
    public Long save(IncorrectLogins incorrectLogins) {
        logger.info("IncorrectLoingsDao::save: " + incorrectLogins);

        entityManager.persist(incorrectLogins);

        return incorrectLogins.getId();
    }

    @Override
    public void update(IncorrectLogins incorrectLogins) {
        logger.info("IncorrectLoingsDao::update: " + incorrectLogins);

        entityManager.merge(incorrectLogins);
    }

    @Override
    public IncorrectLogins get(Long id) {
        logger.info("IncorrectLoingsDao::get:id: " + id);

        return entityManager.find(IncorrectLogins.class, id);
    }

    @Override
    public void delete(Long id) {
        logger.info("IncorrectLoingsDao::delete:id: " + id);

        IncorrectLogins incorrectLogins = entityManager.find(IncorrectLogins.class, id);
        entityManager.remove(incorrectLogins);
    }
}
