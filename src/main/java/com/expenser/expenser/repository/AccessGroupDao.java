package com.expenser.expenser.repository;

import com.expenser.expenser.model.AccessGroup;
import com.expenser.expenser.repository.base.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("accessGroupDao")
@Transactional
public class AccessGroupDao implements BaseRepository<AccessGroup> {

    private static final Logger logger = LoggerFactory.getLogger(AccessGroupDao.class);

    @PersistenceContext
    EntityManager entityManager;


    @Override
    @SuppressWarnings("unchecked")
    public List<AccessGroup> list() {
        logger.info("AccessGroupDao::list");

        return entityManager.createQuery("from AccessGroup").getResultList();
    }

    @Override
    public Long save(AccessGroup accessGroup) {
        logger.info("AccessGroupDao::save: " + accessGroup);

        entityManager.persist(accessGroup);

        return accessGroup.getId();
    }

    @Override
    public void update(AccessGroup accessGroup) {
        logger.info("AccessGroupDao::update: " + accessGroup);

        entityManager.merge(accessGroup);
    }

    @Override
    public AccessGroup get(Long id) {
        logger.info("AccessGroupDao::get:id " + id);

        return entityManager.find(AccessGroup.class, id);
    }

    @Override
    public void delete(Long id) {
        logger.info("AccessGroupDao::delete:id: " + id);

        AccessGroup accessGroup = entityManager.find(AccessGroup.class, id);
        entityManager.remove(accessGroup);
    }
}
