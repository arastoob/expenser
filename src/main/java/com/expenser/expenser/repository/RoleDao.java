package com.expenser.expenser.repository;

import com.expenser.expenser.model.Role;
import com.expenser.expenser.repository.base.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("roleDao")
@Transactional
public class RoleDao implements BaseRepository<Role> {

    private static final Logger logger = LoggerFactory.getLogger(RoleDao.class);

    @PersistenceContext
    EntityManager entityManager;


    @Override
    public List<Role> list() {
        logger.info("RoleDao::list");

        return entityManager.createQuery("from Role").getResultList();
    }

    @Override
    public Long save(Role role) {
        logger.info("RoleDao::save: " + role);

        entityManager.persist(role);

        return role.getId();
    }

    @Override
    public void update(Role role) {
        logger.info("RoleDao::update: " + role);

        entityManager.merge(role);
    }

    @Override
    public Role get(Long id) {
        logger.info("RoleDao::get:id: " + id);
        return entityManager.find(Role.class, id);
    }

    @Override
    public void delete(Long id) {
        logger.info("RoleDao::delete:id: " + id);

        Role role = entityManager.find(Role.class, id);
        entityManager.remove(role);
    }
}
