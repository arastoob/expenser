/*
 * Developed by Arastoo Bozorgi.
 * a.bozorgi67@gmail.com
 */

package com.expenser.expenser.repository;

import com.expenser.expenser.model.LoginUser;
import com.expenser.expenser.model.Role;
import com.expenser.expenser.repository.base.BaseRepository;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * The LoginUserDao repository class which implements BaseRepository interface methods
 */
@Repository("loginUserDao")
@Transactional
public class LoginUserDao implements BaseRepository<LoginUser> {

    private static final Logger logger = LoggerFactory.getLogger(LoginUserDao.class);


    @PersistenceContext
    private EntityManager entityManager;


    /**
     * retrieving a list of LoginUsers
     * @return List<LoginUser>
     */
    @Override
    public List<LoginUser> list() {
        logger.info("LoginUserDao::list");

        return entityManager.createQuery("from LoginUser").getResultList();
    }


    /**
     * creating an LoginUser
     * @param loginUser
     */
    @Override
    public Long save(LoginUser loginUser) {
        logger.info("LoginUserDao::save: " + loginUser);

        entityManager.persist(loginUser);

        return loginUser.getId();
    }


    /**
     * updating an existing LoginUser
     * @param loginUser
     */
    @Override
    public void update(LoginUser loginUser) {
        logger.info("LoginUserDao::update: " + loginUser);

        entityManager.merge(loginUser);
    }


    /**
     * retrieving a specific LoginUser by its id
     * @param id
     * @return
     */
    @Override
    public LoginUser get(Long id) {
        logger.info("LoginUserDao::get:id: " + id);

        return entityManager.find(LoginUser.class, id);
    }


    /**
     * deleting a specific LoginUser by its id
     * @param id
     */
    @Override
    public void delete(Long id) {
        logger.info("LoginUserDao::delete:id: " + id);

        LoginUser loginUser = entityManager.find(LoginUser.class, id);
        entityManager.remove(loginUser);
    }
}
