package com.expenser.expenser.repository;

import com.expenser.expenser.model.User;
import com.expenser.expenser.repository.base.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component("userDao")
@Transactional
public class UserDao implements BaseRepository<User> {

    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> list() {
        logger.info("UserDao::list");

        return entityManager.createQuery("from User").getResultList();
    }

    @Override
    public Long save(User user) {
        logger.info("UserDao::save: " + user);

        entityManager.persist(user);

        return user.getId();
    }

    @Override
    public void update(User user) {
        logger.info("UserDao::update: " + user);

        entityManager.merge(user);
    }

    @Override
    public User get(Long id) {
        logger.info("UserDao::get:id: " + id);

        return entityManager.find(User.class, id);
    }

    @Override
    public void delete(Long id) {
        logger.info("UserDao::delete:id: " + id);

        User user = entityManager.find(User.class, id);
        entityManager.remove(user);
    }
}
