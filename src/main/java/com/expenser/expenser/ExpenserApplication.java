package com.expenser.expenser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ExpenserApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExpenserApplication.class, args);
	}

}
