/*
 * Developed by Arastoo Bozorgi.
 * a.bozorgi67@gmail.com
 */

package com.expenser.expenser.metadata;

import com.expenser.expenser.model.Expenses;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * attributes
     */
    private Long id;
    private String firstName;
    private String lastName;
    @JsonProperty("accessGroup")
    private Set<AccessGroupDto> accessGroupDtos;
    @JsonProperty("role")
    private Set<RoleDto> roleDtos;
    @JsonProperty("expense")
    private Set<ExpensesDto> expensesDtos;
    private String username;
    private String password;



    /**
     * constructors
     */
    public UserDto() {
    }

    public UserDto(Long id, String firstName, String lastName, Set<AccessGroupDto> accessGroupDtos, Set<RoleDto> roleDtos, Set<ExpensesDto> expensesDtos, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accessGroupDtos = accessGroupDtos;
        this.roleDtos = roleDtos;
        this.expensesDtos = expensesDtos;
        this.username = username;
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<AccessGroupDto> getAccessGroupDtos() {
        return accessGroupDtos;
    }

    public void setAccessGroupDtos(Set<AccessGroupDto> accessGroupDtos) {
        this.accessGroupDtos = accessGroupDtos;
    }

    public Set<RoleDto> getRoleDtos() {
        return roleDtos;
    }

    public void setRoleDtos(Set<RoleDto> roleDtos) {
        this.roleDtos = roleDtos;
    }

    public Set<ExpensesDto> getExpensesDtos() {
        return expensesDtos;
    }

    public void setExpensesDtos(Set<ExpensesDto> expensesDtos) {
        this.expensesDtos = expensesDtos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accessGroupDtos=" + accessGroupDtos +
                ", roleDtos=" + roleDtos +
                ", expensesDtos=" + expensesDtos +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
