package com.expenser.expenser.metadata;

import com.expenser.expenser.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserGroupDto implements Serializable {

    private static final long serialVersionUID = 1L;


    private long id;
    private String groupName;
    private String address;
    private String description;
    @JsonProperty("user")
    private Set<UserDto> userDtos;
    private Date createdDate;

    public UserGroupDto() {}

    public UserGroupDto(long id, String groupName, String address, String description, Set<UserDto> userDtos, Date createdDate) {
        this.id = id;
        this.groupName = groupName;
        this.address = address;
        this.description = description;
        this.userDtos = userDtos;
        this.createdDate = createdDate;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UserDto> getUserDtos() {
        return userDtos;
    }

    public void setUserDtos(Set<UserDto> userDtos) {
        this.userDtos = userDtos;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public String toString() {
        return "UserGroupDto{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", userDtos=" + userDtos +
                ", createdDate=" + createdDate +
                '}';
    }
}
