package com.expenser.expenser.metadata;

import com.expenser.expenser.model.Expenses;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExpensesDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * attributes
     */
    private long id;
    private String description;
    private float amount;
    private Date date;
    private Expenses.paymentMethod paymentMethod;

    public ExpensesDto() {
    }

    public ExpensesDto(long id, String description, float amount, Date date, Expenses.paymentMethod paymentMethod) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.paymentMethod = paymentMethod;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Expenses.paymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Expenses.paymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    @Override
    public String toString() {
        return "ExpensesDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", paymentMethod=" + paymentMethod +
                '}';
    }
}
